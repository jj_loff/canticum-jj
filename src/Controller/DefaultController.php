<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Model\DefaultModel as DefaultModel;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Bundle\MonologBundle\SwiftMailer;
use Symfony\Component\Dotenv\Dotenv;

/**
 * 
 */
class DefaultController extends Controller {

	/**
	 *
	 * @var Symfony\Contracts\Translation\TranslatorInterface 
	 */
	public $translator;

	/**
	 * @Route("/{_locale}", name="home" )
	 */
	public function index() {
		return $this->render('default/index.html.twig', [
					'controller_name' => 'DefaultController',
		]);
	}

	/**
	 * @Route("/about_{_locale}", name="about")
	 * @return template
	 */
	public function about() {
		return $this->render('default/about.html.twig', [
					'controller_name' => 'DefaultController',
		]);
	}

	/**
	 * @Route("/contact_{_locale}", name="contact")
	 * @return template
	 */
	public function contact() {

		$request = Request::createFromGlobals();
		
		$message = "";
		if($request->query->get('message')){
			$message = $request->query->get('message');
		}

		return $this->render('default/contact.html.twig', [
			'controller_name' => 'DefaultController',
			'message' => $message,
		]);
	}

	/**
	 * @Route("/news_{_locale}", name="news")
	 * @return template
	 */
	public function news() {

		return $this->render('default/news.html.twig', [
					'controller_name' => 'DefaultController',
		]);
	}

	/**
	 * @Route("/sendmail_{_locale}", name="sendmail")
	 * @param type $request
	 */
	public function sendMail(\Swift_Mailer $mailer) {

		$request = Request::createFromGlobals();
		
		$model = new DefaultModel();
		$errors = array();

		if ($model->sendMail_validate($errors)) {
			
			$name = $request->request->get('name');
			$lastname = $request->request->get('lastname');
			$phone = $request->request->get('phone')?$request->request->get('phone'):"";
			$email = $request->request->get('email');
			
			$defaultBody = "Hi, I'd like to know more about the choir, please.";
			$body = $request->request->get('message')?$request->request->get('message'):$defaultBody;
			$subject = "Canticum Novum Info";

			$dotEnv = new Dotenv();
			$dotEnv->load(__DIR__.'/.env');
			$to = getenv('MAILER_TO');
			
			$message = (new \Swift_Message($subject))
					->setSubject($subject)
					->setReturnPath($email)
					->setFrom($email)
					->setSender([$email => $name." ".$lastname])
					->setTo($to)
					->setReplyTo([$email => $name." ".$lastname])
					->setContentType('text/html')
					->setBody($this->renderView('default/infomail.html.twig', [
						'body' => $body,
						'name' => $name,
						'lastname' => $lastname,
						'phone' => $phone,
						'email' => $email,
					]),'text/html');

			$mailer->send($message);
			
			$confirmation_message = (new \Swift_Message($subject))
					->setSubject($subject)
					->setFrom($to, "Canticum Novum")
					->setSender([$to => "Canticum Novum"])
					->setTo($email)
					->setContentType('text/html')
					->setBody($this->renderView('default/confirmation_email.html.twig', [
						'name' => $name,
						'lastname' => $lastname,
					]),'text/html');
			
			$mailer->send($confirmation_message);
			
			return $this->redirectToRoute("contact",['message' => $this->get('translator')->trans('success.message')]);
			
		} else {
			
			return $this->redirectToRoute("contact");
		}
	}

}
