<?php

namespace App\Model;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Translation\Translator;

/**
 * Description of DefaultModel
 *
 * @author Juergen Johann Loff
 * @created 08 Feb 2019
 */
class DefaultModel{

	/**
	 * 
	 * @param Symfony\Component\HttpFoundation\Request $request
	 * @param array &$errors
	 * @param \TranslatorInterface $translator
	 */
	public function sendMail_validate(array &$errors) {
		
		$request = Request::createFromGlobals();
		$locale = $request->attributes->get('_locale');
		
		$translator = new Translator($locale);

		if (!$request->query->get('name')) {
			$errors['name'] = $translator->trans('name.required');
		}
		if (!$request->query->get('lastname')) {
			$errors['lastname'] = $translator->trans('lastname.required');
		}
		if (!$request->query->get('email')) {
			$errors['email'] = $translator->trans('email.required');
		}
		
		return true;
	}

}
